import time, psutil, subprocess as sub
def getPid(name):
    try:
        rawpid = str((sub.check_output(["pidof", name])))
        return int(rawpid[2:-3:])
    except:
        return -1

def pidTime(process):
    return psutil.Process(getPid(process)).create_time()

if time.time() - pidTime("tilix") < 1.8 and getPid("tmux") != -1:
    sub.call("tmux list-sessions", shell=True)
    session = str(input("$ "))
    if session == "k" or session == "K":
        sub.call("pkill tmux", shell=True)
        sub.call("tmux new", shell=True)
    elif session != "n" and session != "N":
        sub.call("tmux attach -t " + session, shell=True)
    else:
        sub.call("tmux new", shell=True)
else:
    sub.call("tmux new", shell=True)

