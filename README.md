# memux
Launch tilix with managed tmux multiplexing for greater flexibility and safety.
To configure, edit your tilix profile by changing the launch command to start main.py, in the "Command" tab.
